//
//  LocalizationHelper.swift
//  RecipesApp
//
//  Created by user on 16.05.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

class LocalizationHelper {
    
    public static let KEY_RECIPES_TITLE = "controller_recipes_title"
    public static let KEY_RECIPE_CARD_BUTTON = "controller_recipe_button_go_to_author"
    
    public static let KEY_GLOBAL_CONNECTION_ERROR = "global_connection_error"
    
    private static let DEFAULT_COMMENT = ""
    
    public static func getText(_ key: String) -> String {
        return NSLocalizedString(key, comment: LocalizationHelper.DEFAULT_COMMENT)
    }
}
