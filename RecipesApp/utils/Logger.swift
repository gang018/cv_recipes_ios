//
//  Logger.swift
//  RecipesApp
//
//  Created by Roman on 25.11.17.
//  Copyright © 2017 Roman. All rights reserved.
//

import Foundation

class Logger {
    
    static var sDEBUG: Bool {
        #if DEBUG
            return true;
        #else
            return false;
        #endif
    }
    
    init() {
    }
    
    class func log(_ str: Any) {
        if (sDEBUG) {
            print(str);
        }
    }
}
