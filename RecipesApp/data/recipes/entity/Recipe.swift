//
//  Recipe.swift
//  RecipesApp
//
//  Created by user on 27.11.2017.
//  Copyright © 2017 Roman. All rights reserved.
//

import Foundation
import JASON

class Recipe {
    
    var recipe_id: String?;
    var publisher: String?;
    var title: String?;
    var image_url: String?;
    var social_rank: String?;
    var publisher_url: String?;
    
    init(json: JSON) {
        recipe_id = json["recipe_id"].string
        publisher = json["publisher"].string
        title = json["title"].string
        image_url = json["image_url"].string
        social_rank = json["social_rank"].string
        publisher_url = json["publisher_url"].string
    }
    
    init() {
        
    }
    
    deinit {
    }
}
