//
//  RecipesResponse.swift
//  RecipesApp
//
//  Created by Roman on 08.04.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import JASON

class RecipesResponse {
    
    private static let ZERO: Int = 0
    
    var mCount: Int
    var mRecipes: Array<Recipe>
    
    init(json: JSON) {
        mCount = json["count"].intValue
        
        mRecipes = Array<Recipe>()
        guard mCount == RecipesResponse.ZERO else {
            let recipesArray: [JSON] = json["recipes"].jsonArrayValue
            for item in recipesArray {
                mRecipes.append(Recipe(json: item))
            }
            return
        }
    }
    
    func getRecipes() -> Array<Recipe>? {
        return mRecipes
    }
    
    deinit {
        mRecipes.removeAll()
    }
}
