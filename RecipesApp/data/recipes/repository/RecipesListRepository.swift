//
//  RecipesListRepository.swift
//  RecipesApp
//
//  Created by user on 13.03.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import PromiseKit

public class RecipesListRepository : RecipesListRepositoryProtocol {
    
    private static let ZERO: Int = 0
    private var mMemoryCache: RecipesDataSourceProtocol?
    
    init() {
        
    }
    
    func loadRecipes() -> Promise<Array<Recipe>?> {
        return Promise()
            .then { params in
                RecipesDataSourceFactory.createCloudDataSource()
                    .loadRecipes()
                    .map { result in
                        if result != nil && result!.count > RecipesListRepository.ZERO {
                            self.getCacheSource()
                                .saveRecipes(result!)
                        }
                        
                        return result
                }
            }
    }
    
    private func getCacheSource() -> RecipesDataSourceProtocol {
        if mMemoryCache == nil {
            mMemoryCache = RecipesDataSourceFactory.createCacheSource()
        }
        
        return mMemoryCache!
    }
    
    func loadRecipe(_ id: String) -> Promise<Recipe?> {
        return getCacheSource().loadRecipe(id)
    }
    
    deinit {
        
    }
}
