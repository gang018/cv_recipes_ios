//
//  RecipesCloudDataSource.swift
//  RecipesApp
//
//  Created by user on 13.03.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import JASON

class RecipesCloudDataSource : RecipesDataSourceProtocol {
    
    private static let API_URL = "http://food2fork.com/api/"
    private static let API_KEY = "026e11996a0cc33b052a7924e4f46757"
    
    private static let API_SEARCH = API_URL + "search?key=" + API_KEY
    
    init() {
        
    }
    
    func loadRecipes() -> Promise<Array<Recipe>?> {
        let q = DispatchQueue.global()
        return Promise()
            .then { result in
                return Alamofire.request(RecipesCloudDataSource.API_SEARCH)
                    .responseJSON()
            }.map(on: q) { result in
                Logger.log("mapping...")
                let parsedData = JSON(result.json)
                return RecipesResponse(json: parsedData)
                    .getRecipes()
            }
    }
    
    func loadRecipe(_ id: String) -> Promise<Recipe?> {
        let q = DispatchQueue.global()
        return Promise()
            .map(on: q) { result in
                return nil
            }
    }
    
    func saveRecipes(_ recipes: Array<Recipe>) {
        // ignore
    }
    
    deinit {
        
    }
}
