//
//  RecipesDataSourceProtocol.swift
//  RecipesApp
//
//  Created by user on 13.03.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import PromiseKit

protocol RecipesDataSourceProtocol {
    func loadRecipes() -> Promise<Array<Recipe>?>
    
    func saveRecipes(_ recipes: Array<Recipe>)
    
    func loadRecipe(_ id: String) -> Promise<Recipe?>
}
