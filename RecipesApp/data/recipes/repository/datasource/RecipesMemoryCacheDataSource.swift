//
//  RecipesMemoryCacheDataSource.swift
//  RecipesApp
//
//  Created by user on 15.05.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import PromiseKit

class RecipesMemoryCacheDataSource: RecipesDataSourceProtocol {
    
    private var mRecipes: Array<Recipe>?
    
    func loadRecipes() -> Promise<Array<Recipe>?> {
        let q = DispatchQueue.global()
        return Promise()
            .map(on: q) { result in
                return self.mRecipes
            }
    }
    
    func saveRecipes(_ recipes: Array<Recipe>) {
        mRecipes = recipes
    }
    
    func loadRecipe(_ id: String) -> Promise<Recipe?> {
        let q = DispatchQueue.global()
        return Promise()
            .map(on: q) { result in
                if self.mRecipes == nil {
                    return nil
                }
                
                for item in self.mRecipes! {
                    if item.recipe_id!.elementsEqual(id) {
                        return item
                    }
                }
                
                return nil
            }
    }
    
    deinit {
        mRecipes = nil
    }
}
