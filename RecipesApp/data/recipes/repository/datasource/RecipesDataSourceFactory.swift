//
//  RecipesDataSourceFactory.swift
//  RecipesApp
//
//  Created by user on 14.03.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

class RecipesDataSourceFactory {
    
    class func createCloudDataSource() -> RecipesDataSourceProtocol {
        return RecipesCloudDataSource()
    }
    
    class func createCacheSource() -> RecipesDataSourceProtocol {
        return RecipesMemoryCacheDataSource()
    }
}
