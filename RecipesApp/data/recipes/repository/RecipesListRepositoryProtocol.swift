//
//  RecipesListProtocol.swift
//  RecipesApp
//
//  Created by user on 01.12.2017.
//  Copyright © 2017 Roman. All rights reserved.
//

import Foundation
import PromiseKit

protocol RecipesListRepositoryProtocol {
    func loadRecipes() -> Promise<Array<Recipe>?>
    
    func loadRecipe(_ id: String) -> Promise<Recipe?>
}
