//
//  RecipesInteractorProtocol.swift
//  RecipesApp
//
//  Created by user on 20.04.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import PromiseKit

protocol RecipesInteractorProtocol {
    func loadRecipes() -> Promise<Array<Recipe>?>
    
    func loadRecipe(_ id: String) -> Promise<Recipe?>
}
