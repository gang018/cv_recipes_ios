//
//  RecipesInteractor.swift
//  RecipesApp
//
//  Created by user on 20.04.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import PromiseKit

class RecipesInteractor: RecipesInteractorProtocol {
    
    var mRepository: RecipesListRepositoryProtocol?
    
    init(_ repository: RecipesListRepositoryProtocol) {
        mRepository = repository
    }
    
    func loadRecipes() -> Promise<Array<Recipe>?> {
        return (mRepository?.loadRecipes())!
    }
    
    func loadRecipe(_ id: String) -> Promise<Recipe?> {
        return (mRepository?.loadRecipe(id))!
    }
    
    deinit {
        mRepository = nil
    }
}
