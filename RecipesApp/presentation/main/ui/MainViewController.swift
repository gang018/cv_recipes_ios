//
//  ViewController.swift
//  RecipesApp
//
//  Created by Roman on 25.11.17.
//  Copyright © 2017 Roman. All rights reserved.
//

import UIKit
import PromiseKit

class MainViewController: UINavigationController, MainViewProtocol {

    private var mRouter: MainScreenRouter?
    private var mPresenter: MainPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        mRouter = DIManager.getMainRouter()
        mRouter?.setController(self)
        
        mPresenter = DIManager.getMainPreseenter()
        mPresenter?.bindView(self)
    }
    
    func showFirstScreen() {
        mRouter?.showFirstScreen()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        mRouter = nil
        mPresenter = nil
    }
}
