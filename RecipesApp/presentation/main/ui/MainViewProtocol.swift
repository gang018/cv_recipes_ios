//
//  MainViewProtocol.swift
//  RecipesApp
//
//  Created by user on 16.05.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

protocol MainViewProtocol {
    func showFirstScreen()
}
