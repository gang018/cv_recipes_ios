//
//  MainPresenter.swift
//  RecipesApp
//
//  Created by user on 16.05.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

class MainPresenter: MainPresenterProtocol {
    
    private var mView: MainViewProtocol?
    
    func bindView(_ view: MainViewProtocol) {
        mView = view;
        mView?.showFirstScreen()
    }
    
    deinit {
        mView = nil
    }
}
