//
//  RecipeCellClickListener.swift
//  RecipesApp
//
//  Created by Roman on 02.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

protocol RecipeCellClickListenerProtocol {
    
    func onItemClick(_ item: Recipe)
}
