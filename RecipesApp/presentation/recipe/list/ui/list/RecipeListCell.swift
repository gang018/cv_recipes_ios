//
//  RecipeListCell.swift
//  RecipesApp
//
//  Created by Roman on 01.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import UIKit

class RecipeListCell: UITableViewCell {
    
    private static let HASHTAG: String = "#"
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(_ recipe: Recipe) {
        textLabel?.text = recipe.title
        detailTextLabel?.text = RecipeListCell.HASHTAG + recipe.publisher!
    }
}
