//
//  RecipesListView.swift
//  RecipesApp
//
//  Created by Roman on 30.04.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import UIKit

class RecipesListView: UIView {
    
    private static let ZERO: Int = 0
    private static let FONT_SIZE_ERROR: Int = 20
    
    private var mLoaderView: UIActivityIndicatorView?
    private var mErrorLabel: UILabel?
    private var mListView: UITableView?
    private var mListDataSource: RecipesListViewDataSource?
    
    init() {
        let zero: CGFloat = CGFloat(RecipesListView.ZERO)
        let frame = CGRect(x: zero,
                           y: zero,
                           width: UIScreen.main.bounds.width,
                           height: UIScreen.main.bounds.height)
        super.init(frame: frame)
        
        initialize()
    }
    
    func initialize() {
        mLoaderView = UIActivityIndicatorView()
        mLoaderView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        mLoaderView?.color = UIColor.darkGray
        let loaderCenterX = UIScreen.main.bounds.midX
        let loaderCenterY = UIScreen.main.bounds.midY
        let loaderSize = CGFloat()
        mLoaderView?.frame = CGRect(x: loaderCenterX, y: loaderCenterY, width: loaderSize, height: loaderSize)
        addSubview(mLoaderView!)
        
        
        mErrorLabel = UILabel()
        mErrorLabel?.text = LocalizationHelper.getText(LocalizationHelper.KEY_GLOBAL_CONNECTION_ERROR)
        mErrorLabel?.textColor = UIColor.lightGray
        mErrorLabel?.textAlignment = NSTextAlignment.center
        mErrorLabel?.isUserInteractionEnabled = true
        mErrorLabel?.isHidden = true
        mErrorLabel?.numberOfLines = RecipesListView.ZERO
        mErrorLabel?.font = mErrorLabel?.font.withSize(CGFloat(RecipesListView.FONT_SIZE_ERROR))
        let zeroPoint = CGFloat();
        mErrorLabel?.frame = CGRect(x: zeroPoint,
                                    y: zeroPoint,
                                    width: UIScreen.main.bounds.width,
                                    height: UIScreen.main.bounds.height)
        addSubview(mErrorLabel!)
        
        
        let listFrame: CGRect = CGRect(x: zeroPoint,
                                       y: zeroPoint,
                                       width: UIScreen.main.bounds.width,
                                       height: UIScreen.main.bounds.height)
        mListView = UITableView(frame: listFrame, style: UITableViewStyle.plain)
        mListView?.isHidden = true
        mListView?.backgroundColor = UIColor.clear
        mListDataSource = RecipesListViewDataSource()
        mListView?.dataSource = mListDataSource
        mListView?.delegate = mListDataSource
        mListView?.register(RecipeListCell.self,
                            forCellReuseIdentifier: RecipesListViewDataSource.CELL_IDENTIFIER)
        addSubview(mListView!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setLoaderVisible(_ visible: Bool) {
        mLoaderView?.isHidden = !visible;
        if visible {
            mLoaderView?.startAnimating()
        } else {
            mLoaderView?.stopAnimating()
        }
    }
    
    func setErrorVisiblie(_ visible: Bool) {
        mErrorLabel?.isHidden = !visible
    }
    
    func setErrorClickListener(_ listener: UITapGestureRecognizer) {
        mErrorLabel?.addGestureRecognizer(listener)
    }
    
    func setData(_ data: Array<Recipe>) {
        mListDataSource?.setItems(data)
        mListView?.reloadData()
    }
    
    func setDataVisible(_ visible: Bool) {
        mListView?.isHidden = !visible
    }
    
    func setListClickListener(_ listener: RecipeCellClickListenerProtocol) {
        mListDataSource?.setClickListener(listener)
    }
    
    deinit {
        mLoaderView = nil
        mErrorLabel = nil
        mListView = nil
        mListDataSource = nil
    }
}
