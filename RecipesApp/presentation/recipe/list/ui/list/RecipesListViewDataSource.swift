//
//  RecipesListViewDelegate.swift
//  RecipesApp
//
//  Created by Roman on 01.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import UIKit

class RecipesListViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    public static let CELL_IDENTIFIER: String = "identifier"
    
    private static let ZERO: Int = 0
    
    private var mItems: Array<Recipe>?
    private var mClickListener: RecipeCellClickListenerProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let items = mItems {
            return items.count
        }
        
        return RecipesListViewDataSource.ZERO
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RecipesListViewDataSource.CELL_IDENTIFIER,
                                                 for: indexPath) as! RecipeListCell
        let recipe = mItems?[indexPath.item]
        cell.setData(recipe!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recipe = mItems?[indexPath.item]
        mClickListener?.onItemClick(recipe!)
    }
    
    func setClickListener(_ listener: RecipeCellClickListenerProtocol) {
        mClickListener = listener
    }
    
    func setItems(_ items: Array<Recipe>) {
        mItems = items
    }
    
    deinit {
        mItems?.removeAll()
        mItems = nil
        mClickListener = nil
    }
}
