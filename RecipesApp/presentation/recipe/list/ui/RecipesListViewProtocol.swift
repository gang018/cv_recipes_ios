//
//  RecipesListViewProtocol.swift
//  RecipesApp
//
//  Created by Roman on 29.04.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

protocol RecipesListViewProtocol {
    
    func setLoaderVisible(_ visible: Bool)
    
    func setErrorVisible(_ visible: Bool)
    
    func setData(_ data: Array<Recipe>)
    
    func setDataVisible(_ visible: Bool)
}
