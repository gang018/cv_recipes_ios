//
//  RecipesListViewController.swift
//  RecipesApp
//
//  Created by Roman on 29.04.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import UIKit
import Foundation

class RecipesListViewController: UIViewController, RecipesListViewProtocol, RecipeCellClickListenerProtocol {
    
    private var mPresenter: RecipesListPresenterProtocol?
    private var mView: RecipesListView!
    private var mRouter: MainScreenRouter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mView = RecipesListView()
        mView?.setErrorClickListener(
            UITapGestureRecognizer(target: self, action: #selector(refreshButtonClicked))
        )
        mView?.setListClickListener(self)
        view.addSubview(mView)
        
        mRouter = DIManager.getMainRouter()
        mPresenter = DIManager.getRecipeListPresenter()
        mPresenter?.bindView(self)
    }
    
    @objc func refreshButtonClicked() {
        mPresenter?.handleRefreshClick()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.title = LocalizationHelper.getText(LocalizationHelper.KEY_RECIPES_TITLE)
    }
    
    func onItemClick(_ item: Recipe) {
        mRouter?.routeToRecipeCard(item.recipe_id!)
    }
    
    func setLoaderVisible(_ visible: Bool) {
        mView?.setLoaderVisible(visible)
    }
    
    func setErrorVisible(_ visible: Bool) {
        mView?.setErrorVisiblie(visible)
    }
    
    func setData(_ data: Array<Recipe>) {
        mView?.setData(data)
    }
    
    func setDataVisible(_ visible: Bool) {
        mView?.setDataVisible(visible)
    }
    
    deinit {
        mPresenter = nil
        mView = nil
        mRouter = nil
    }
}
