//
//  RecipesListPresenter.swift
//  RecipesApp
//
//  Created by Roman on 29.04.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

class RecipesListPresenter: RecipesListPresenterProtocol {
    
    private var mInteractor: RecipesInteractorProtocol?
    private var mView: RecipesListViewProtocol?
    
    init(_ interactor: RecipesInteractorProtocol) {
        mInteractor = interactor
    }
    
    func bindView(_ view: RecipesListViewProtocol) {
        mView = view
        loadData()
    }
    
    private func loadData() {
        mView?.setLoaderVisible(true)
        mView?.setErrorVisible(false)
        mView?.setDataVisible(false)
        mInteractor?.loadRecipes()
            .done { result in
                guard result != nil else {
                    Logger.log("finished success with 0")
                    self.mView?.setErrorVisible(true)
                    return
                }
                
                self.mView?.setDataVisible(true)
                self.mView?.setData(result!)
                Logger.log("finished success")
            }.catch { error in
                Logger.log("finished error")
                self.mView?.setErrorVisible(true)
            }.finally {
                self.mView?.setLoaderVisible(false)
            }
    }
    
    func handleRefreshClick() {
        loadData()
    }
    
    deinit {
        mView = nil
        mInteractor = nil
    }
}
