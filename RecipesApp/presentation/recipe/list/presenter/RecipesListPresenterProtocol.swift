//
//  RecipesListPresenterProtocol.swift
//  RecipesApp
//
//  Created by Roman on 29.04.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

protocol RecipesListPresenterProtocol {
    
    func bindView(_ view: RecipesListViewProtocol)
    
    func handleRefreshClick()
}
