//
//  RecipeCardView.swift
//  RecipesApp
//
//  Created by Roman on 06.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

class RecipeCardView: UIView {
    
    private static let ZERO = 0
    private static let IMAGE_SIZE_MULT: CGFloat = 0.4
    private static let TITLE_PADDING: CGFloat = 7
    private static let MULTIPLIER_DEFAULT: CGFloat = 1
    private static let TITLE_FONT_SIZE: CGFloat = 20
    private static let DESCRIPTION_FONT_SIZE: CGFloat = 16
    private static let MULTIPLIER_BUTTON_SIDE: CGFloat = 0.9
    private static let MULTIPLIER_BUTTON_BOTTOM: CGFloat = 0.94
    private static let BUTTON_CORNER_RADIUS: CGFloat = 5
    private static let FONT_SIZE_ERROR: Int = 20
    private static let COLOR_PRIMARY_BLUE: UIColor = UIColor(red: 83/255, green: 109/255, blue: 254/255, alpha: 1)
    
    private static let TEXT_HASHTAG = "#"
    
    private var mTopImage: UIImageView?
    private var mTitleLabel: UITextView?
    private var mDescriptionLabel: UITextView?
    private var mButton: UIButton?
    private var mLoaderView: UIActivityIndicatorView?
    private var mErrorView: UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init() {
        let zeroPoint = CGFloat(RecipeCardView.ZERO)
        let rect = CGRect(x: zeroPoint, y: zeroPoint,
                          width: UIScreen.main.bounds.width,
                          height: UIScreen.main.bounds.height);
        
        super.init(frame: rect)
        
        initialize()
    }
    
    private func initialize() {
        backgroundColor = UIColor.white
        translatesAutoresizingMaskIntoConstraints = true
        
        
        mLoaderView = UIActivityIndicatorView()
        mLoaderView?.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        mLoaderView?.color = UIColor.darkGray
        mLoaderView?.translatesAutoresizingMaskIntoConstraints = false
        addSubview(mLoaderView!)
        
        let zeroPoint = CGFloat(RecipeCardView.ZERO)
        let loaderWidthConstraint = NSLayoutConstraint(item: mLoaderView!, attribute: .width, relatedBy: .equal,
                                                       toItem: self, attribute: .width,
                                                       multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        let loaderHeightConstraint = NSLayoutConstraint(item: mLoaderView!, attribute: .height, relatedBy: .equal,
                                                        toItem: self, attribute: .height,
                                                        multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        NSLayoutConstraint.activate([loaderWidthConstraint, loaderHeightConstraint])
        
        
        mErrorView = UILabel()
        mErrorView?.text = LocalizationHelper.getText(LocalizationHelper.KEY_GLOBAL_CONNECTION_ERROR)
        mErrorView?.textColor = UIColor.lightGray
        mErrorView?.textAlignment = NSTextAlignment.center
        mErrorView?.translatesAutoresizingMaskIntoConstraints = false
        mErrorView?.isUserInteractionEnabled = true
        mErrorView?.isHidden = true
        mErrorView?.numberOfLines = RecipeCardView.ZERO
        mErrorView?.font = mErrorView?.font.withSize(CGFloat(RecipeCardView.FONT_SIZE_ERROR))
        addSubview(mErrorView!)
        
        let errorViewWidthConstraint = NSLayoutConstraint(item: mErrorView!, attribute: .width, relatedBy: .equal,
                                                       toItem: self, attribute: .width,
                                                       multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        let errorViewHeightConstraint = NSLayoutConstraint(item: mErrorView!, attribute: .height, relatedBy: .equal,
                                                        toItem: self, attribute: .height,
                                                        multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        NSLayoutConstraint.activate([errorViewWidthConstraint, errorViewHeightConstraint])
        
        
        mTopImage = UIImageView()
        mTopImage?.isHidden = true
        mTopImage?.translatesAutoresizingMaskIntoConstraints = false
        mTopImage?.backgroundColor = UIColor.lightGray
        addSubview(mTopImage!)
        
        let imageWidthConstraint = NSLayoutConstraint(item: mTopImage!, attribute: .width, relatedBy: .equal,
                                                          toItem: self, attribute: .width,
                                                          multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        let imageHeightConstraint = NSLayoutConstraint(item: mTopImage!, attribute: .height, relatedBy: .equal,
                                                           toItem: self, attribute: .height,
                                                           multiplier: RecipeCardView.IMAGE_SIZE_MULT, constant: zeroPoint)
        NSLayoutConstraint.activate([imageWidthConstraint, imageHeightConstraint])
        
        
        mTitleLabel = UITextView()
        mTitleLabel?.translatesAutoresizingMaskIntoConstraints = false
        mTitleLabel?.isScrollEnabled = false
        mTitleLabel?.isHidden = true
        mTitleLabel?.isUserInteractionEnabled = false
        mTitleLabel?.font = .systemFont(ofSize: RecipeCardView.TITLE_FONT_SIZE)
        mTitleLabel?.textContainerInset = UIEdgeInsets(top: RecipeCardView.TITLE_PADDING,
                                                       left: RecipeCardView.TITLE_PADDING,
                                                       bottom: RecipeCardView.TITLE_PADDING,
                                                       right: RecipeCardView.TITLE_PADDING)
        addSubview(mTitleLabel!)
        let titleTopConstraint = NSLayoutConstraint(item: mTitleLabel!, attribute: .top, relatedBy: .equal,
                                               toItem: mTopImage!, attribute: .bottom,
                                               multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        
        let titleWidthConstraint = NSLayoutConstraint(item: mTitleLabel!, attribute: .width, relatedBy: .equal,
                                                 toItem: mTopImage!, attribute: .width,
                                                 multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        
        NSLayoutConstraint.activate([titleTopConstraint, titleWidthConstraint])
        
        
        mDescriptionLabel = UITextView()
        mDescriptionLabel?.translatesAutoresizingMaskIntoConstraints = false
        mDescriptionLabel?.isUserInteractionEnabled = false
        mDescriptionLabel?.isScrollEnabled = false
        mDescriptionLabel?.isHidden = true
        mDescriptionLabel?.font = .systemFont(ofSize: RecipeCardView.DESCRIPTION_FONT_SIZE)
        mDescriptionLabel?.textColor = UIColor.darkGray
        mDescriptionLabel?.textContainerInset = UIEdgeInsets(top: RecipeCardView.TITLE_PADDING,
                                                       left: RecipeCardView.TITLE_PADDING,
                                                       bottom: RecipeCardView.TITLE_PADDING,
                                                       right: RecipeCardView.TITLE_PADDING)
        addSubview(mDescriptionLabel!)
        let descriptionTopConstraint = NSLayoutConstraint(item: mDescriptionLabel!, attribute: .top, relatedBy: .equal,
                                                    toItem: mTitleLabel!, attribute: .bottom,
                                                    multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        
        let descriptionWidthConstraint = NSLayoutConstraint(item: mDescriptionLabel!, attribute: .width, relatedBy: .equal,
                                                      toItem: mTopImage!, attribute: .width,
                                                      multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        
        NSLayoutConstraint.activate([descriptionTopConstraint, descriptionWidthConstraint])
        
        
        mButton = UIButton(type: .custom)
        mButton?.backgroundColor = RecipeCardView.COLOR_PRIMARY_BLUE
        mButton?.layer.cornerRadius = RecipeCardView.BUTTON_CORNER_RADIUS
        mButton?.setTitleColor(UIColor.white, for: .normal)
        mButton?.isHidden = true
        mButton?.setTitle(LocalizationHelper.getText(LocalizationHelper.KEY_RECIPE_CARD_BUTTON), for: UIControlState.normal)
        mButton?.translatesAutoresizingMaskIntoConstraints = false
        let buttonTopPadding = RecipeCardView.TITLE_PADDING * 2
        mButton?.contentEdgeInsets = UIEdgeInsets(top: buttonTopPadding,
                                                  left: zeroPoint,
                                                  bottom: buttonTopPadding,
                                                  right: zeroPoint)
        addSubview(mButton!)
        
        let buttonBottomConstraint = NSLayoutConstraint(item: mButton!, attribute: .bottom, relatedBy: .equal,
                                                          toItem: self, attribute: .bottom,
                                                          multiplier: RecipeCardView.MULTIPLIER_BUTTON_BOTTOM, constant: zeroPoint)
        
        let buttonWidthConstraint = NSLayoutConstraint(item: mButton!, attribute: .width, relatedBy: .equal,
                                                            toItem: mTopImage!, attribute: .width,
                                                            multiplier: RecipeCardView.MULTIPLIER_BUTTON_SIDE, constant: zeroPoint)
        
        let buttonCenterConstraint = NSLayoutConstraint(item: mButton!, attribute: .centerX, relatedBy: .equal,
                                                       toItem: self, attribute: .centerX,
                                                       multiplier: RecipeCardView.MULTIPLIER_DEFAULT, constant: zeroPoint)
        
        NSLayoutConstraint.activate([buttonBottomConstraint, buttonWidthConstraint, buttonCenterConstraint])
    }
    
    public func setTitle(_ title: String?) {
        mTitleLabel?.text = title
    }
    
    public func setDescription(_ text: String?) {
        mDescriptionLabel?.text = RecipeCardView.TEXT_HASHTAG + text!
    }
    
    public func setImage(_ imagePath: String?) {
        mTopImage?.af_setImage(withURL: URL(string: imagePath!)!)
    }
    
    public func setDataVisible(_ visible: Bool) {
        mDescriptionLabel?.isHidden = !visible
        mButton?.isHidden = !visible
        mTopImage?.isHidden = !visible
        mTitleLabel?.isHidden = !visible
    }
    
    public func setLoaderVisible(_ visible: Bool) {
        mLoaderView?.isHidden = !visible
        if !visible {
            mLoaderView?.stopAnimating()
        } else {
            mLoaderView?.startAnimating()
        }
    }
    
    public func setErrorVisible(_ visible: Bool) {
        mErrorView?.isHidden = !visible
    }
    
    public func setErrorClickListener(_ listener: UITapGestureRecognizer) {
        mErrorView?.addGestureRecognizer(listener)
    }
    
    public func setAuthorsPageClickListener(_ listener: UITapGestureRecognizer) {
        mButton?.addGestureRecognizer(listener)
    }
    
    deinit {
        mTopImage = nil
        mTitleLabel = nil
        mDescriptionLabel = nil
        mButton = nil
        mLoaderView = nil
        mErrorView = nil
    }
}
