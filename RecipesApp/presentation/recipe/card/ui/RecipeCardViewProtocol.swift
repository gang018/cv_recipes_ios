//
//  RecipeCardViewProtocol.swift
//  RecipesApp
//
//  Created by Roman on 05.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

protocol RecipeCardViewProtocol {
    func setLoaderVisible(_ visible: Bool)
    
    func setData(_ recipe: Recipe)
    
    func setDataVisible(_ visible: Bool)
    
    func setErrorVisible(_ visible: Bool)
    
    func routeToAuthorsPage(_ url: String)
}
