//
//  RecipeCardController.swift
//  RecipesApp
//
//  Created by Roman on 02.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import UIKit

class RecipeCardController: UIViewController, RecipeCardViewProtocol {
    
    private static let HASHTAG: String = "#"
    
    private var mRecipeID: String?
    private var mView: RecipeCardView?
    private var mRouter: MainScreenRouter?
    private var mPresenter: RecipeCardPresenterProtocol?
    
    override func viewDidLoad() {
        mView = RecipeCardView()
        mView?.setErrorClickListener(UITapGestureRecognizer(target: self, action: #selector(errorClicked)))
        mView?.setAuthorsPageClickListener(UITapGestureRecognizer(target: self, action: #selector(authorPageButtonClick)))
        view.addSubview(mView!)
        self.title = RecipeCardController.HASHTAG + mRecipeID!
        
        mRouter = DIManager.getMainRouter()
        mPresenter = DIManager.getRecipeCardPresenter()
        mPresenter?.bindView(self, mRecipeID!)
    }
    
    @objc private func errorClicked() {
        mPresenter?.handleErrorClicked()
    }
    
    @objc private func authorPageButtonClick() {
        mPresenter?.handleAuthorsPageClicked()
    }
    
    func setRecipeId(_ recipeId: String) {
        mRecipeID = recipeId
    }
    
    func setLoaderVisible(_ visible: Bool) {
        mView?.setLoaderVisible(visible)
    }
    
    func setDataVisible(_ visible: Bool) {
        mView?.setDataVisible(visible)
    }
    
    func setData(_ recipe: Recipe) {
        mView?.setTitle(recipe.title)
        mView?.setDescription(recipe.publisher)
        mView?.setImage(recipe.image_url)
    }
    
    func setErrorVisible(_ visible: Bool) {
        mView?.setErrorVisible(visible)
    }
    
    func routeToAuthorsPage(_ strUrl: String) {
        mRouter?.routeToAuthorsPage(strUrl)
    }
    
    deinit {
        mRouter = nil
        mRecipeID = nil
        mPresenter = nil
        mView = nil
    }
}
