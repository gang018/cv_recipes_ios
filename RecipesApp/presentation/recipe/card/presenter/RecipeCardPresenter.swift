//
//  RecipeCardPresenter.swift
//  RecipesApp
//
//  Created by Roman on 05.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

class RecipeCardPresenter: RecipeCardPresenterProtocol {
    
    private var mView: RecipeCardViewProtocol?
    private var mInteractor: RecipesInteractorProtocol?
    private var mRecipeId: String?
    private var mRecipe: Recipe?
    
    init(_ interactor: RecipesInteractorProtocol) {
        mInteractor = interactor
    }
    
    func bindView(_ view: RecipeCardViewProtocol, _ recipeId: String) {
        mView = view
        mRecipeId = recipeId
        
        loadData()
    }
    
    private func loadData() {
        mView?.setDataVisible(false)
        mView?.setErrorVisible(false)
        mView?.setLoaderVisible(true)
        
        mInteractor?.loadRecipe(mRecipeId!)
            .done { recipe in
                guard recipe != nil else {
                    self.mView?.setErrorVisible(true)
                    return
                }
                self.mRecipe = recipe
                self.mView?.setData(recipe!)
                self.mView?.setDataVisible(true)
            }.catch { error in
                self.mView?.setErrorVisible(true)
            }.finally {
                self.mView?.setLoaderVisible(false)
        }
    }
    
    func handleErrorClicked() {
        loadData()
    }
    
    func handleAuthorsPageClicked() {
        mView?.routeToAuthorsPage((mRecipe?.publisher_url)!)
    }
    
    deinit {
        mInteractor = nil
        mView = nil
        mRecipe = nil
        mRecipeId = nil
    }
}
