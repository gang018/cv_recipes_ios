//
//  RecipeCardPresenterProtocol.swift
//  RecipesApp
//
//  Created by Roman on 05.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

protocol RecipeCardPresenterProtocol {
    
    func bindView(_ view: RecipeCardViewProtocol, _ recipeId: String)
    
    func handleErrorClicked()
    
    func handleAuthorsPageClicked()
}
