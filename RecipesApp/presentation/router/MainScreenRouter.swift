//
//  MainScreenRouter.swift
//  RecipesApp
//
//  Created by Roman on 02.05.18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation
import UIKit

class MainScreenRouter {
    
    private var mMainController: MainViewController?
    
    init() {
        
    }
    
    public func setController(_ controller: MainViewController) {
        mMainController = controller
    }
    
    public func showFirstScreen() {
        let recipesListController = RecipesListViewController()
        mMainController?.pushViewController(recipesListController, animated: false)
    }
    
    public func routeToRecipeCard(_ recipeId: String) {
        let recipeCardController = RecipeCardController()
        recipeCardController.setRecipeId(recipeId)
        mMainController?.pushViewController(recipeCardController, animated: true)
    }
    
    public func routeBack() {
        mMainController?.popViewController(animated: true)
    }
    
    public func routeToAuthorsPage(_ strUrl: String) {
        let url = URL(string: strUrl)
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    deinit {
        mMainController = nil
    }
}
