//
//  DIManager.swift
//  RecipesApp
//
//  Created by user on 15.05.2018.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

class DIManager {
    
    private static var mRecipesRepository: RecipesListRepositoryProtocol?
    private static var mRouter: MainScreenRouter?
    
    public static func getRecipeListPresenter() -> RecipesListPresenterProtocol {
        return RecipesListPresenter(getRecipesListInteractor())
    }
    
    private static func getRecipesListInteractor() -> RecipesInteractorProtocol {
        return RecipesInteractor(getRecipesRepository())
    }
    
    private static func getRecipesRepository() -> RecipesListRepositoryProtocol {
        if mRecipesRepository == nil {
            mRecipesRepository = RecipesListRepository()
        }
        
        return mRecipesRepository!
    }
    
    public static func getRecipeCardPresenter() -> RecipeCardPresenterProtocol {
        return RecipeCardPresenter(getRecipesListInteractor())
    }
    
    public static func getMainPreseenter() -> MainPresenterProtocol {
        return MainPresenter()
    }
    
    public static func getMainRouter() -> MainScreenRouter {
        if mRouter == nil {
            mRouter = MainScreenRouter()
        }
        
        return mRouter!
    }
}
