#### Стек технологий:
* Xcode 
* Swift 4+
* MVP + Clean Architecture
* Alamofire
* AlamofireImage
* PromiseKit

Приложение отображает список рецептов, по нажатию на рецепт открывается подробное описание рецепта.

![Screenshot 1](https://bitbucket.org/gang018/cv_recipes_ios/raw/fe298bbe0a4a18eda0f90f7e1ed2fb1aa154fb5f/screenshots/list.png)
![Screenshot 2](https://bitbucket.org/gang018/cv_recipes_ios/raw/fe298bbe0a4a18eda0f90f7e1ed2fb1aa154fb5f/screenshots/card.png)
